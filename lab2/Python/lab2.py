# -*- coding: utf-8 -*-

#%%
# import various libraries necessery to run your Python code
import time   # time related library
import sys    # system related library
import atexit
ok_loc = 'C:\\Program Files\\Opal Kelly\\FrontPanelUSB\\API\\Python\\3.6\\x64'
sys.path.append(ok_loc)   # add the path of the OK library
import ok     # OpalKelly library

#%% 
# Define FrontPanel device variable, open USB communication and
# load the bit file in the FPGA
dev = ok.okCFrontPanel()  # define a device for FrontPanel communication
SerialStatus=dev.OpenBySerial("")      # open USB communicaiton with the OK board
ConfigStatus=dev.ConfigureFPGA("..\\lab2.runs\\\\impl_1\\lab2_example.bit"); # Configure the FPGA with this bit file

# Check if FrontPanel is initialized correctly and if the bit file is loaded.
# Otherwise terminate the program
print("----------------------------------------------------")
if SerialStatus == 0:
    print ("FrontPanel host interface was successfully initialized.")
else:    
    print ("FrontPanel host interface not detected. The error code number is:" + str(int(SerialStatus)))
    print("Exiting the program.")
    sys.exit ()

if ConfigStatus == 0:
    print ("Your bit file is successfully loaded in the FPGA.")
else:
    print ("Your bit file did not load. The error code number is:" + str(int(ConfigStatus)))
    print ("Exiting the progam.")
    sys.exit ()
print("----------------------------------------------------")
print("----------------------------------------------------")

type = 0
while type not in (1,2):
	type = int(input("Exercise 1 or 2?: "))
	
freq = 5
if type == 2:
	freq = int(input("Enter frequency (Hz): "))
dev.SetWireInValue(0x01, freq)
dev.SetWireInValue(0x00, 0)
dev.UpdateWireIns()

def exit_handler():
	dev.Close
	print("closing connection...")
atexit.register(exit_handler)


while 1:
	counter = dev.GetWireOutValue(0x21)
	dev.UpdateWireOuts()
	print("\rCounter: {:03} ".format(counter), end="")
	reset = int(counter >= 100 and type == 2)
	dev.SetWireInValue(0x00, reset)
	dev.UpdateWireIns()
	time.sleep(0.1)


#%%