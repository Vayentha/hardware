# -*- coding: utf-8 -*-

#%%
# import various libraries necessery to run your Python code
import time   # time related library
import sys    # system related library
ok_loc = 'C:\\Program Files\\Opal Kelly\\FrontPanelUSB\\API\\Python\\3.6\\x64'
sys.path.append(ok_loc)   # add the path of the OK library
import ok     # OpalKelly library
import pickle
from pprint import pprint
#%% 
# Define FrontPanel device variable, open USB communication and
# load the bit file in the FPGA
dev = ok.okCFrontPanel()  # define a device for FrontPanel communication
SerialStatus=dev.OpenBySerial("")      # open USB communicaiton with the OK board
ConfigStatus=dev.ConfigureFPGA("..\\lab11.runs\\impl_1\\toplevel.bit"); # Configure the FPGA with this bit file

# Check if FrontPanel is initialized correctly and if the bit file is loaded.
# Otherwise terminate the program
print("----------------------------------------------------")
if SerialStatus == 0:
    print ("FrontPanel host interface was successfully initialized.")
else:    
    print ("FrontPanel host interface not detected. The error code number is:" + str(int(SerialStatus)))
    print("Exiting the program.")
    sys.exit ()

if ConfigStatus == 0:
    print ("Your bit file is successfully loaded in the FPGA.")
else:
    print ("Your bit file did not load. The error code number is:" + str(int(ConfigStatus)))
    print ("Exiting the progam.")
    sys.exit ()
print("----------------------------------------------------")
print("----------------------------------------------------")

#print("Press Button 3 to begin data collection!")
time.sleep(1)

def twos_comp(val, bits):
    """compute the 2's complement of int value val"""
    if (val & (1 << (bits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << bits)        # compute negative value
    return val

def run_motor(pulses, direction):
    dev.SetWireInValue(0x00, pulses)
    dev.SetWireInValue(0x01, direction)
    dev.SetWireInValue(0x02, 0)
    dev.UpdateWireIns()
    dev.SetWireInValue(0x02, 1)
    dev.UpdateWireIns()

acc_scaling_factor = (9.8 * 2)/(2**15)
mag_scaling_factor = 1.3/(2**15) 

while(1):
    steps = int(input("Steps: "))
    dir = int(input("Dir : "))
    run_motor(steps,dir)
    for i in range(20):
        dev.UpdateWireOuts()
        read_a = lambda x:(twos_comp(dev.GetWireOutValue(x), 16)*acc_scaling_factor)
        read_m = lambda x:(twos_comp(dev.GetWireOutValue(x), 16)*mag_scaling_factor)
        x_acc = read_a(0x20)
        y_acc = read_a(0x21)
        z_acc = read_a(0x22) 
        x_mag = read_m(0x23)
        y_mag = read_m(0x24)
        z_mag = read_m(0x25)
        print("acceleration data:")
        print("X:{:5.3f}g  Y:{:5.3f}g  Z:{:5.3f}g".format(x_acc,y_acc,z_acc))
        print("magnetic data:")
        print("X:{:5.3f}G  Y:{:5.3f}G  Z:{:5.3f}G".format(x_mag,y_mag,z_mag))	
        print()

gathered_data = []
for i in range(10):
    run_motor(100,0)
    run_data = []
    start = time.clock()
    for i in range(400):
        dev.UpdateWireOuts()
        read_a = lambda x:(twos_comp(dev.GetWireOutValue(x), 16)*acc_scaling_factor)
        read_m = lambda x:(twos_comp(dev.GetWireOutValue(x), 16)*mag_scaling_factor)
        x_acc = read_a(0x20)
        y_acc = read_a(0x21)
        z_acc = read_a(0x22) 
        x_mag = read_m(0x23)
        y_mag = read_m(0x24)
        z_mag = read_m(0x25)
        run_data.append((x_acc,y_acc,z_acc))
    stop = time.clock()
    print((stop-start)*1000.0)
    gathered_data.append(run_data)
    time.sleep(1.5)
    run_motor(120,1)
    time.sleep(1.5)
with open("gathered_data_.pickle", "wb") as f:
    pickle.dump(gathered_data, f)

dev.Close
    