`timescale 1ns / 1ps

module JTEG_Test_File(

    input   wire    [4:0] okUH,
    output  wire    [2:0] okHU,
    inout   wire    [31:0] okUHU,
    inout   wire    okAA,
    
    input [3:0] button,
    output [7:0] led,
    input sys_clkn,
    input sys_clkp,  
    output I2C_SCL_1,
    inout I2C_SDA_1 

);
/*
   reg sys_clkn=1;
    wire sys_clkp;
    wire [7:0] led;
    reg [3:0] button;
*/

    wire okClk;            //These are FrontPanel wires needed to IO communication    
    wire [112:0]    okHE;  //These are FrontPanel wires needed to IO communication    
    wire [64:0]     okEH;  //These are FrontPanel wires needed to IO communication    
             
    //This is the OK host that allows data to be sent or recived    
    okHost hostIF (
        .okUH(okUH),
        .okHU(okHU),
        .okUHU(okUHU),
        .okClk(okClk),
        .okAA(okAA),
        .okHE(okHE),
        .okEH(okEH)
    );
    
    //Depending on the number of outgoing endpoints, adjust endPt_count accordingly.
    localparam  endPt_count = 6;
    wire [endPt_count*65-1:0] okEHx;  
    okWireOR # (.N(endPt_count)) wireOR (okEH, okEHx);
    
    
    wire  ILA_Clk, ACK_bit, FSM_Clk, TrigerEvent;    
    wire [23:0] ClkDivThreshold = 1_000;   
    wire SCL, SDA; 
    wire [7:0] State;
    wire [15:0] SDA_data;
    
    wire [15:0] acc_x;
    wire [15:0] acc_y;
    wire [15:0] acc_z;
    wire [15:0] mag_x;
    wire [15:0] mag_y;
    wire [15:0] mag_z;
   
    
          
    okWireOut wire20 (  .okHE(okHE), 
                        .okEH(okEHx[ 0*65 +: 65 ]),
                        .ep_addr(8'h20), 
                        .ep_datain(acc_x));
    okWireOut wire21 (  .okHE(okHE), 
                        .okEH(okEHx[ 1*65 +: 65 ]),
                        .ep_addr(8'h21), 
                        .ep_datain(acc_y));
    okWireOut wire22 (  .okHE(okHE), 
                        .okEH(okEHx[ 2*65 +: 65 ]),
                        .ep_addr(8'h22), 
                        .ep_datain(acc_z));                                                                 
    okWireOut wire23 (  .okHE(okHE), 
                        .okEH(okEHx[ 3*65 +: 65 ]),
                        .ep_addr(8'h23), 
                        .ep_datain(mag_x));  
    okWireOut wire24 (  .okHE(okHE), 
                        .okEH(okEHx[ 4*65 +: 65 ]),
                        .ep_addr(8'h24), 
                        .ep_datain(mag_y));
    okWireOut wire25 (  .okHE(okHE), 
                        .okEH(okEHx[ 5*65 +: 65 ]),
                        .ep_addr(8'h25), 
                        .ep_datain(mag_z));     
                                              
    assign TrigerEvent = button[3];

    

    //Instantiate the module that we like to test
    I2C_Transmit I2C_Test1 (
        .button(button),
        .led(led),
        .sys_clkn(sys_clkn),
        .sys_clkp(sys_clkp),
        .I2C_SCL_1(I2C_SCL_1),
        .I2C_SDA_1(I2C_SDA_1),             
        .FSM_Clk_reg(FSM_Clk),        
        .ILA_Clk_reg(ILA_Clk),
        .ACK_bit(ACK_bit),
        .SCL(SCL),
        .SDA(SDA),
        .state_out(State),
        .SDA_data_out(SDA_data),
        
        .acc_x(acc_x),
        .acc_y(acc_y),
        .acc_z(acc_z),
        .mag_x(mag_x),
        .mag_y(mag_y),
        .mag_z(mag_z)
        );
    
    //Instantiate the ILA module
    ila_0 ila_sample12 ( 
        .clk(ILA_Clk),
        .probe0({led, SDA, SCL, ACK_bit}),                             
        .probe1({FSM_Clk, TrigerEvent}),
        .probe2(State),
        .probe3(SDA_data)
        );  
        

     /*
    assign sys_clkp = ~sys_clkn;    
    always begin
        #5 sys_clkn = ~sys_clkn;
    end        
      
    initial begin          
            #0 button <= 4'b1111;                                                      
            #200000 button <= 4'b0000;           
    end   
        */        
endmodule