`timescale 1ns / 1ps

module I2C_Transmit(
    input [3:0] button,
    output [7:0] led,
    input sys_clkn,
    input sys_clkp,
    output ADT7420_A0,
    output ADT7420_A1,
    output I2C_SCL_0,
    inout  I2C_SDA_0,        
    output reg FSM_Clk_reg,    
    output reg ILA_Clk_reg,
    output reg ACK_bit,
    output reg SCL,
    output reg SDA, 
    output [7:0] state_out,
    output reg [15:0] SDA_data_out
    );
    
    //Instantiate the ClockGenerator module, where three signals are generate:
    //High speed CLK signal, Low speed FSM_Clk signal     
    wire [23:0] ClkDivThreshold = 100;   
    wire FSM_Clk, ILA_Clk; 
    ClockGenerator ClockGenerator1 (  .sys_clkn(sys_clkn),
                                      .sys_clkp(sys_clkp),                                      
                                      .ClkDivThreshold(ClkDivThreshold),
                                      .FSM_Clk(FSM_Clk),                                      
                                      .ILA_Clk(ILA_Clk) );
                                        
    reg [7:0] SingleByteData = 8'b1001_0001;
    reg [7:0] State = 8'd0;
    reg [3:0] button_reg; 
    reg error_bit = 1'b1;     
    reg Run_scl;
    reg [15:0] SDA_data;
    reg [7:0] Data_recieved;
    reg [7:0] SCL_pulse;
       
    localparam STATE_INIT       = 8'd0;    
    assign led[7] = ACK_bit;
    assign led[6] = error_bit;
    assign ADT7420_A0 = 1'b0;
    assign ADT7420_A1 = 1'b0;
    assign I2C_SCL_0 = SCL;
    assign I2C_SDA_0 = SDA; 
    
    initial  begin
        SDA_data_out = 16'd0;
        ACK_bit = 1'b1;   
        SDA_data = 16'b0;
        SCL_pulse = 8'b0;
        Run_scl = 1'b0;
    end
    
    always @(*) begin
        button_reg = ~button;  
        FSM_Clk_reg = FSM_Clk;
        ILA_Clk_reg = ILA_Clk;   
    end   
                 
    assign state_out = State; 
    reg [7:0] device_write = 8'b1001_0000;
    reg [7:0] device_read = 8'b1001_0001;
    reg [7:0] registerA    = 8'b0000_0000;
    reg [7:0] registerB    = 8'b0000_0001;
          
    always @* begin
        SCL = ~Run_scl | SCL_pulse == 1 | SCL_pulse == 2;
        SDA = 1'b1;
        if ( Run_scl )  begin
            // start
            if ( State == 0 ) SDA = 1'b0;
            // device write 
            if ( State > 0 ) SDA = device_write[8-State];
            // ack
            if ( State == 9 ) SDA = 1'bz;
            // register select
            if ( State > 9 ) SDA = registerA[17-State];
            // ack
            if ( State == 18 ) SDA = 1'bz;
            // start high
            if ( State == 19 ) SDA = 1'b1;
            // start transition
            if ( State == 19 && SCL_pulse > 1) SDA = 1'b0;
            // device read (high byte)
            if ( State > 19 ) SDA = device_read[27-State];
            // ack
            if ( State > 27 ) SDA = 1'bz;
            // read byte in states 29-36
            
            // ack
            if ( State == 37 ) SDA = 1'b0;
            // start high
            if ( State == 38 ) SDA = 1'b1;
            // start transition
            if ( State == 38 && SCL_pulse > 1) SDA = 1'b0;
            // device read (low byte)
            if ( State > 38 ) SDA = device_read[46-State];
            // ack
            if ( State > 46 ) SDA = 1'bz;
            // read byte in states 48-55
            
            // nack
            if ( State == 56 ) SDA = 1'd1;
            // stop low
            if ( State == 57 ) SDA = 1'b0;
            // stop transition
            if ( State == 57 && SCL_pulse > 1) SDA = 1'b1;
            // done
            if ( State > 57 ) SDA = 1'b1;  
        end
    end
                     
    always @(posedge FSM_Clk) begin 
        SCL_pulse <= SCL_pulse + 1;
        if ( SCL_pulse == 3 ) SCL_pulse <= 0;
        if ( button_reg[3] == 1'b1 && Run_scl == 1'b0 )begin
            Run_scl <= 1'b1;
            SCL_pulse <= 1'b1;
        end
        if ( Run_scl && SCL_pulse == 3 ) begin
            State <= State + 1;
        end

        if ( State > 28 && State < 37 && SCL_pulse == 1) begin
            // read top byte
            SDA_data[36-State+8] <= SDA;
        end
        if ( State > 47 && State < 56 && SCL_pulse == 1) begin
            // read bottom byte
            SDA_data[55-State] <= SDA;
        end        
        if ( State == 65 ) begin
            //Run_scl <= 1'b0;
            SDA_data_out <= (SDA_data >>> 3);
            SCL_pulse <= 1'b1;
            State <= 8'd0;
        end          
    end                     
endmodule
