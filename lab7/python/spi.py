# -*- coding: utf-8 -*-

#%%
# import various libraries necessery to run your Python code
import time   # time related library
import sys    # system related library
import random
ok_loc = 'C:\\Program Files\\Opal Kelly\\FrontPanelUSB\\API\\Python\\3.6\\x64'
sys.path.append(ok_loc)   # add the path of the OK library
import ok     # OpalKelly library

#%% 
# Define FrontPanel device variable, open USB communication and
# load the bit file in the FPGA
dev = ok.okCFrontPanel()  # define a device for FrontPanel communication
SerialStatus=dev.OpenBySerial("")      # open USB communicaiton with the OK board
ConfigStatus=dev.ConfigureFPGA("..\\lab7.runs\\impl_1\\toplevel.bit"); # Configure the FPGA with this bit file

# Check if FrontPanel is initialized correctly and if the bit file is loaded.
# Otherwise terminate the program
print("----------------------------------------------------")
if SerialStatus == 0:
    print ("FrontPanel host interface was successfully initialized.")
else:    
    print ("FrontPanel host interface not detected. The error code number is:" + str(int(SerialStatus)))
    print("Exiting the program.")
    sys.exit ()

if ConfigStatus == 0:
    print ("Your bit file is successfully loaded in the FPGA.")
else:
    print ("Your bit file did not load. The error code number is:" + str(int(ConfigStatus)))
    print ("Exiting the progam.")
    sys.exit ()
print("----------------------------------------------------")
print("----------------------------------------------------")

def readspi(addr, burst):
	dev.SetWireInValue(0x00, 0) # data
	dev.SetWireInValue(0x01, addr) # addr
	dev.SetWireInValue(0x02, 0) # read/write
	dev.SetWireInValue(0x03, 0) 
	dev.SetWireInValue(0x04, burst)
	dev.UpdateWireIns()
	dev.SetWireInValue(0x03, 1) # start
	dev.UpdateWireIns()
	#time.sleep(0.01)
	dev.UpdateWireOuts()
	return dev.GetWireOutValue(0x20)
	
def writespi(addr, data):
	dev.SetWireInValue(0x00, data) # data
	dev.SetWireInValue(0x01, addr) # addr
	dev.SetWireInValue(0x02, 1) # read/write
	dev.SetWireInValue(0x03, 0) 
	dev.SetWireInValue(0x04, 0)
	dev.UpdateWireIns()
	dev.SetWireInValue(0x03, 1) # start
	dev.UpdateWireIns()
	return


rand = random.randint(100,200)
print("Reading register 3:")
print(readspi(3,0))
print("Setting register 3 to {}....".format(rand))
writespi(3,rand)
print("Reading register 3 again:")
print(readspi(3,0))
print()

CLOCK_FREQ = 25.0 # Clock frequency in Mhz
	
while 1:
	"""
	# try reading register 1 and 2 at the same time (Burst mode)
	print(readspi(78, 1))
	# try reading register 1 and 2 seperately
	print(readspi(78, 0))
	print(readspi(79, 0))
	print()
	"""

	temp = (float((readspi(79,0) << 8) | readspi(78,0)) - (4900*CLOCK_FREQ/25)	) / (13*CLOCK_FREQ/25)
	print(temp)

	time.sleep(1)

dev.Close
