`timescale 1ns / 1ps

module toplevel(

    output CVM300_CLK_IN,
    
    output CVM300_SPI_EN,
    output CVM300_SPI_CLK,
    output CVM300_SPI_IN,
    input  CVM300_SPI_OUT,
    
    input   wire    [4:0] okUH,
    output  wire    [2:0] okHU,
    inout   wire    [31:0] okUHU,
    inout   wire    okAA,
    
    output wire [7:0] USER_33,
    
    input [3:0] button,
    output [7:0] led,
    input sys_clkn,
    input sys_clkp

    );

  /*  
    reg sys_clkn=1;
    wire sys_clkp;
    wire [7:0] led;
    reg  Read_Write;  // 0 == read, 1 == write.
    reg  [6:0] Addr;
    reg  Start;
    wire  [7:0] Data_out;
    wire  [15:0] Data_in;
    reg  Burst;
  */
    
  
    wire  Read_Write;  // 0 == read, 1 == write.
    wire  [6:0] Addr;
    wire  Start;
    wire  [7:0] Data_out;
    wire  [15:0] Data_in;
    wire  Burst;
    wire FSM_Clk;
    
    assign USER_33[7:1] = 7'b0;
    assign USER_33[0] = FSM_Clk;

    wire okClk;            //These are FrontPanel wires needed to IO communication    
    wire [112:0]    okHE;  //These are FrontPanel wires needed to IO communication    
    wire [64:0]     okEH;  //These are FrontPanel wires needed to IO communication    
           
            
    //This is the OK host that allows data to be sent or recived    
    okHost hostIF (
        .okUH(okUH),
        .okHU(okHU),
        .okUHU(okUHU),
        .okClk(okClk),
        .okAA(okAA),
        .okHE(okHE),
        .okEH(okEH)
    );
    
    //Depending on the number of outgoing endpoints, adjust endPt_count accordingly.
    localparam  endPt_count = 1;
    wire [endPt_count*65-1:0] okEHx;  
    okWireOR # (.N(endPt_count)) wireOR (okEH, okEHx);
   
    okWireIn wire0 (    .okHE(okHE), 
                        .ep_addr(8'h00), 
                        .ep_dataout(Data_out));
                        
     okWireIn wire1 (   .okHE(okHE), 
                        .ep_addr(8'h01), 
                        .ep_dataout(Addr));
                                               
    okWireIn wire2 (    .okHE(okHE), 
                        .ep_addr(8'h02), 
                        .ep_dataout(Read_Write));
                        
    okWireIn wire3 (    .okHE(okHE), 
                        .ep_addr(8'h03), 
                        .ep_dataout(Start));  
                        
    okWireIn wire4 (    .okHE(okHE), 
                        .ep_addr(8'h04), 
                        .ep_dataout(Burst));       
                                        
    // SPI Data is transmited to the PC via address 0x20   
    okWireOut wire20 (  .okHE(okHE), 
                        .okEH(okEHx[ 0*65 +: 65 ]),
                        .ep_addr(8'h20), 
                        .ep_datain(Data_in));
 
     /*
    
    Register map
    
    0x00 SPI Data from PC
    0x01 SPI Address
    0x02 SPI Read/Write
    0x03 SPI Start
    0x04 SPI Burst Mode
    
    0x20 SPI Data to PC
    
    */
       
SPI_comm SPI_comm(
                  .CVM300_CLK_IN(CVM300_CLK_IN),

                  .CVM300_SPI_EN(CVM300_SPI_EN),
                  .CVM300_SPI_CLK(CVM300_SPI_CLK),
                  .CVM300_SPI_IN(CVM300_SPI_IN),
                  .CVM300_SPI_OUT(CVM300_SPI_OUT),
                  
                  .Read_Write(Read_Write),
                  .Addr(Addr),
                  .Start(Start),
                  .Data_out(Data_out),
                  .Data_in(Data_in),
                  .Burst(Burst),
                  
                  .button(button),
                  .led(led),
                  .sys_clkn(sys_clkn),
                  .sys_clkp(sys_clkp),
                  
                  .FSM_Clk(FSM_Clk)
                  );

/*
    assign sys_clkp = ~sys_clkn;   
    always begin
        #5 sys_clkn = ~sys_clkn;
    end        
      
    initial begin         
            #0 Start <= 1'b0; 
               Read_Write <= 1'b0;
               Addr <= 7'b1010101;
               Burst <= 1'b1;                                                  
            #20000 Start <= 1'b1;           
    end   
                        
    */          
endmodule
