`timescale 1ns / 1ps

module SPI_comm(
    output CVM300_CLK_IN,

    output CVM300_SPI_EN,
    output CVM300_SPI_CLK,
    output reg CVM300_SPI_IN,
    input CVM300_SPI_OUT,
    
    input  Read_Write, // 0 == read, 1 == write.
    input  [6:0] Addr,
    input Start,
    input Burst,
    input [7:0] Data_out,
    output reg  [15:0] Data_in,
    output FSM_Clk,

    input [3:0] button,
    output [7:0] led,
    input sys_clkn,
    input sys_clkp
    );
    
    //Instantiate the ClockGenerator module, where three signals are generate:
    //High speed CLK signal, Low speed FSM_Clk signal     
    wire [23:0] ClkDivThreshold = 3; // 3 -> 25Mhz. 4 -> 20Mhz.
    wire FSM_Clk, ILA_Clk; 
    ClockGenerator ClockGenerator1 (  .sys_clkn(sys_clkn),
                                      .sys_clkp(sys_clkp),                                      
                                      .ClkDivThreshold(ClkDivThreshold),
                                      .FSM_Clk(FSM_Clk),                                      
                                      .ILA_Clk(ILA_Clk) );
                                        
    reg [7:0] State;
    reg Running; 
    reg Done;
    wire [6:0] NextAddr;
    
    initial  begin
        State = 8'b0;
        Running = 1'b0;
        Done = 1'b1;
        Data_in = 16'b0;
    end
          
    assign CVM300_SPI_CLK = Running && State < (16<<Burst) ? FSM_Clk : 1'b0;
    assign CVM300_CLK_IN = FSM_Clk;
    assign CVM300_SPI_EN = Running;
    assign NextAddr = Addr+1;
    
    always @* begin
       if ( State == 0 ) CVM300_SPI_IN = Read_Write;
       if ( State > 0 ) CVM300_SPI_IN = Addr[7 - State];
       if ( State > 7 ) CVM300_SPI_IN = ( Read_Write ? Data_out[15 - State] : 1'b0 );
       if ( State > 15) CVM300_SPI_IN <= 1'b0;
       
       if ( Burst && State == 16 ) CVM300_SPI_IN = Read_Write;
       if ( Burst && State > 16 ) CVM300_SPI_IN = NextAddr[23 - State];
       if ( Burst && State > 23 ) CVM300_SPI_IN = ( Read_Write ? Data_out[31 - State] : 1'b0 );
       if ( Burst && State > 31) CVM300_SPI_IN <= 1'b0;
    end
                            
    always @(negedge FSM_Clk) begin 
        if ( Running && State < (16<<Burst)) State <= State+1;
        if ( State >= (16<<Burst) )  Running <= 1'b0;
        if ( !Running && !Start )  Done <= 1'b1;
           
        if ( !Running && Start && Done ) begin
            Running <= 1'b1;
            State <= 8'b0;
            Done <= 1'b0;
        end
    end  
    
    always @(posedge FSM_Clk) begin
        if ( Running && State == 8'b1 ) begin
            Data_in <= 16'b0;
        end
        if ( Running && State > 7 && State < 16) begin
            Data_in[15 - State] <= CVM300_SPI_OUT;
        end 
        if ( Running && State > 23 && State < 32 && Burst ) begin
            Data_in[39 - State] <= CVM300_SPI_OUT;
        end
    end     
                  
endmodule
