`timescale 1ns / 1ps

module lab3_TestBench();
              
    reg sys_clkn=1;
    wire sys_clkp;
    wire [7:0] led;
    reg [3:0] button;
    
    //Invoke the module that we like to test
    lab3_example ModuleUnderTest (.button(button),.led(led),.sys_clkn(sys_clkn),.sys_clkp(sys_clkp));
        
    assign sys_clkp = ~sys_clkn;    
    always begin
        #5 sys_clkn = ~sys_clkn;
    end        
      
    initial begin          
            #0 button <= 4'b1111;                                                      
            #1000 button <= 4'b1110;
            #100  button <= 4'b1111;
           
    end

endmodule
