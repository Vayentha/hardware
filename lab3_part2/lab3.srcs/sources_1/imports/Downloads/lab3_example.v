`timescale 1ns / 1ps
module lab3_example(
    input [3:0] button,
    output [7:0] led,
    input sys_clkn,
    input sys_clkp  
    );
    
    reg [7:0] state = 0;
    reg [7:0] led_register = 0;
    reg [3:0] button_reg;    
    reg ped_cross;
    reg delay;
    reg R1,Y1,G1,R2,Y2,G2,R3,G3;    
    reg slow_clk;
    reg [31:0] clkdiv ;  
    reg ped_clear;         
    reg ped_clearstop;
                
    wire clk;
    IBUFGDS osc_clk(
        .O(clk),
        .I(sys_clkp),
        .IB(sys_clkn)
    );
    
    initial begin
        slow_clk = 0;
        clkdiv = 0;
        ped_cross = 0;
        state = 8'd0;
        ped_clear = 0;
        ped_clearstop = 0;
    end
  
    assign led = ~led_register; //map led wire to led_register  
    
    always @(posedge clk)
    begin       
        clkdiv <= clkdiv + 1'b1;
        if (clkdiv >= (32'd100000000)) begin
        //if (clkdiv >= (32'd10)) begin
            slow_clk <= ~slow_clk;
            clkdiv <= 0;
            
        led_register <= {
        R1,
        Y1,
        G1,
        R2,
        Y2,
        G2,
        R3,
        G3
        };

        delay <= 1'b0;
        
        case (state)
            8'd0: begin
                if ( delay ) begin
                    ped_clear <= 1'b0;
                    state <= 8'd1;
                end else begin
                    delay <= 1'b1;
                    ped_clear <= 1'b1;
                end
            end
            8'd1: begin 
                state <= 8'd2;
            end
            8'd2: begin
                if ( ped_cross ) begin
                    state <= 8'd3;
                end else begin
                    state <= 8'd4;
                end
            end
            8'd3: begin
                if ( delay ) begin
                    state <= 8'd4;
                end else begin
                    delay <= 1'b1;
                    ped_cross <= 1'b0;
                end
            end
            8'd4: begin
                if ( delay ) begin
                    ped_clear <= 1'b0;
                    state <= 8'd5;
                end else begin
                    delay <= 1'b1;
                    ped_clear <= 1'b1;
                end
            end
            8'd5: begin
                state <= 8'd6;
            end
            8'd6: begin
                if ( ped_cross ) begin
                    state <= 8'd7;
                end else begin
                    state <= 8'd0;
                end
            end
            8'd7: begin
                if ( delay ) begin
                    state <= 8'd0;
                end else begin
                    delay <= 1'b1;
                    ped_cross <= 1'b0;
                end
            end
        endcase            
          
        G3 = ( state == 8'd3 || state == 8'd7);
        R3 = ~G3;

        if ( state == 8'd0 ) begin
            G1 = 1'b1;
            Y1 = 1'b0;
            R1 = 1'b0;
        end else if ( state == 8'd1 ) begin
            G1 = 1'b0;
            Y1 = 1'b1;
            R1 = 1'b0;
        end else begin
            G1 = 1'b0;
            Y1 = 1'b0;
            R1 = 1'b1;
        end
         
        if ( state == 8'd4 ) begin
            G2 = 1'b1;
            Y2 = 1'b0;
            R2 = 1'b0;
        end else if ( state == 8'd5 ) begin
            G2 = 1'b0;
            Y2 = 1'b1;
            R2 = 1'b0;
        end else begin
            G2 = 1'b0;
            Y2 = 1'b0;
            R2 = 1'b1;
        end
      end    
      
        if(button == 4'b1110) begin
            ped_cross <= 1'b1;
        end 
    end    
endmodule

