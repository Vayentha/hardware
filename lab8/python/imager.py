# import various libraries necessery to run your Python code
import sys    # system related library
ok_loc = 'C:\\Program Files\\Opal Kelly\\FrontPanelUSB\\API\\Python\\3.6\\x64'
sys.path.append(ok_loc)   # add the path of the OK library
import ok,time,pickle     # OpalKelly library
import numpy as np
from PIL import Image

#%% 
# Define FrontPanel device variable, open USB communication and
# load the bit file in the FPGA
dev = ok.okCFrontPanel()  # define a device for FrontPanel communication
SerialStatus=dev.OpenBySerial("")      # open USB communicaiton with the OK board
ConfigStatus=dev.ConfigureFPGA("..\\lab8.runs\\impl_1\\imager.bit") # Configure the FPGA with this bit file

# Check if FrontPanel is initialized correctly and if the bit file is loaded.
# Otherwise terminate the program
print("----------------------------------------------------")
if SerialStatus == 0:
    print ("FrontPanel host interface was successfully initialized.")
else:    
    print ("FrontPanel host interface not detected. The error code number is:" + str(int(SerialStatus)))
    print("Exiting the program.")
    sys.exit ()

if ConfigStatus == 0:
    print ("Your bit file is successfully loaded in the FPGA.")
else:
    print ("Your bit file did not load. The error code number is:" + str(int(ConfigStatus)))
    print ("Exiting the progam.")
    sys.exit ()
print("----------------------------------------------------")
print("----------------------------------------------------")

def readspi(addr):
	dev.SetWireInValue(0x00, 0) # data
	dev.SetWireInValue(0x01, addr) # addr
	dev.SetWireInValue(0x02, 0) # read/write
	dev.SetWireInValue(0x03, 0) 
	dev.UpdateWireIns()
	dev.SetWireInValue(0x03, 1) # start
	dev.UpdateWireIns()
	#time.sleep(0.01)
	dev.UpdateWireOuts()
	return dev.GetWireOutValue(0x20)
	
def writespi(addr, data):
	dev.SetWireInValue(0x00, data) # data
	dev.SetWireInValue(0x01, addr) # addr
	dev.SetWireInValue(0x02, 1) # read/write
	dev.SetWireInValue(0x03, 0) 
	dev.UpdateWireIns()
	dev.SetWireInValue(0x03, 1) # start
	dev.UpdateWireIns()
	return

	
writespi(69,9) # enable clock out
writespi(83,187) # set pll range to 20.83<->41.67Mhz

writespi(58,44)

# bottom offset
writespi(59,240)
writespi(60,10)
#writespi(59,0)
#writespi(60,0)

# PGA gain. set to 3 for 1.75x amplification
writespi(80,3)

# top offset
writespi(97,240)
writespi(98,10)
#writespi(97,0)
#writespi(98,0)

# ADC gain
# for 25Mhz set to 51+64
writespi(100,51+64)

writespi(101,98)
writespi(102,34)
writespi(103,64)
writespi(106,94)
writespi(107,110)
writespi(108,91)
writespi(109,82)
writespi(110,80)
writespi(117,91)

# set to Parallel out mode
writespi(57,3)

# increase the exposure time
writespi(42,255)
writespi(43,5) 

dev.SetWireInValue(0x04, 1) #Reset FIFOs and counter
dev.UpdateWireIns()  # Update the WireIns

s = 315392
buf = bytearray(s) # close to 648*488 but it has to be a multiple of 1024

dev.SetWireInValue(0x04, 0) #Release reset signal
dev.UpdateWireIns()  # Update the WireIns

dev.ReadFromBlockPipeOut(0xa0, 1024, buf)

data_exists = False
for x in buf:
	if x != 0:
		data_exists = True
		break
print("Data exists: {}".format(data_exists))

with open( "image.pickle", "wb" ) as f:
	pickle.dump( buf, f)

for i in range(0,len(buf)//4):
	for j in range(0,2):
		x = (i*4)+j
		y = (i*4)+3-j
		buf[x] ^= buf[y]
		buf[y] ^= buf[x]
		buf[x] ^= buf[y]

image = Image.frombytes('L', (648,486), bytes(buf), 'raw')
image_cropped = image.crop((2,2,642,482))
image_cropped.load()
image_cropped.show()
image_cropped.save("sensor_output.png")
