import pickle
from PIL import Image
buf = None
with open( "image.pickle", "rb" ) as f:
	buf = pickle.load(f)
#------ the rest of the code is the same as the imager code ------

for i in range(0,len(buf)//4):
	for j in range(0,2):
		x = (i*4)+j
		y = (i*4)+3-j
		buf[x] ^= buf[y]
		buf[y] ^= buf[x]
		buf[x] ^= buf[y]

image = Image.frombytes('L', (648,486), bytes(buf), 'raw')
image_cropped = image.crop((2,2,642,482))
image_cropped.load()
image_cropped.show()
image_cropped.save("sensor_output.png")
