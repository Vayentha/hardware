`timescale 1ns / 1ps

 module imager(

    
    input [9:0] CVM300_D,
    input CVM300_Line_valid,
    input CVM300_Data_valid,
    input CVM300_CLK_OUT,
    output reg CVM300_FRAME_REQ,
    output reg CVM300_SYS_RES_N,
    output CVM300_Enable_LVDS,

    output CVM300_CLK_IN,
    output CVM300_SPI_EN,
    output CVM300_SPI_CLK,
    output reg CVM300_SPI_IN,
    input CVM300_SPI_OUT,
    
    output wire [7:0] USER_33,

    input   wire    [4:0] okUH,
    output  wire    [2:0] okHU,
    inout   wire    [31:0] okUHU,
    inout   wire    okAA,
    input [3:0] button,
    output [7:0] led,
    input sys_clkn,
    input sys_clkp
    );
    
    wire okClk;            //These are FrontPanel wires needed to IO communication    
    wire [112:0]    okHE;  //These are FrontPanel wires needed to IO communication    
    wire [64:0]     okEH;  //These are FrontPanel wires needed to IO communication     
    //This is the OK host that allows data to be sent or recived    
    okHost hostIF (
        .okUH(okUH),
        .okHU(okHU),
        .okUHU(okUHU),
        .okClk(okClk),
        .okAA(okAA),
        .okHE(okHE),
        .okEH(okEH)
    );
        
    //Depending on the number of outgoing endpoints, adjust endPt_count accordingly.
    //In this example, we have 1 output endpoints, hence endPt_count = 1.
    localparam  endPt_count = 2;
    wire [endPt_count*65-1:0] okEHx;  
    okWireOR # (.N(endPt_count)) wireOR (okEH, okEHx);    
    
    //Instantiate the ClockGenerator module, where three signals are generate:
    //High speed CLK signal, Low speed FSM_Clk signal     
    wire [23:0] ClkDivThreshold = 3;   
    wire FSM_Clk, ILA_Clk; 
    
    wire Trigger_event; 
    assign Trigger_event = button[3]; 
    
    ClockGenerator ClockGenerator1 (  .sys_clkn(sys_clkn),
                                      .sys_clkp(sys_clkp),                                      
                                      .ClkDivThreshold(ClkDivThreshold),
                                      .FSM_Clk(FSM_Clk),                                      
                                      .ILA_Clk(ILA_Clk) );
                                                                                  
    localparam STATE_INIT                = 8'd0;
    localparam STATE_RESET               = 8'd1;   
    localparam STATE_DELAY               = 8'd2;
    localparam STATE_RESET_FINISHED      = 8'd3;
    localparam STATE_ENABLE_WRITING      = 8'd4;
    localparam STATE_COUNT               = 8'd5;
    localparam STATE_FINISH              = 8'd6;
   
    reg [7:0] pixel_reg;
   
    reg [31:0] counter = 8'd0;
    reg [15:0] counter_delay = 16'd0;
    reg [7:0] State = STATE_INIT;
    reg [7:0] led_register = 0;
    reg [3:0] button_reg, write_enable_counter;  
    reg write_reset, read_reset, write_enable;
    wire [31:0] Reset_Counter;
    wire [31:0] DATA_Counter;    
    wire FIFO_read_enable, FIFO_BT_BlockSize_Full, FIFO_full, FIFO_empty, BT_Strobe;
    wire [31:0] FIFO_data_out;
    
    wire  Read_Write; // 0 == read, 1 == write.
    wire  [6:0] Addr;
    wire Start; //error found: start signal never set to 1
    wire Burst;
    wire [7:0] Data_out;
    reg  [15:0] Data_in;
    reg  [15:0] State_Reg;
    wire FSM_Clk;
                    
    reg [7:0] State_spi;
    reg Running; 
    reg Done;
    wire [6:0] NextAddr;
    
    initial  begin
        State_spi = 8'b0;
        Running = 1'b0;
        Done = 1'b1;
        //Data_in = 16'b0;
        write_reset <= 1'b0;
        read_reset <= 1'b0;
        write_enable <= 1'b1;   
        CVM300_FRAME_REQ <= 1'b0; 
        CVM300_SYS_RES_N <= 1'b1;
    end
    
    assign led[0] = ~FIFO_empty; 
    assign led[1] = ~FIFO_full;
    assign led[2] = ~FIFO_BT_BlockSize_Full;
    assign led[3] = ~FIFO_read_enable;
    assign led[4] = FIFO_empty; //SPS
    assign led[5] = write_enable; //SPS
    assign led[7] = ~read_reset;
   
    assign CVM300_Enable_LVDS = 1'b0;
    assign USER_33[0] = CVM300_FRAME_REQ;
    assign USER_33[1] = CVM300_D[2];
    assign USER_33[2] = CVM300_CLK_IN;

                                         
    always @(negedge CVM300_CLK_OUT) begin     
        button_reg <= ~button;   // Grab the values from the button, complement and store them in register    
        //State_Reg <= State;           
        if (Reset_Counter[0] == 1'b1) State <= STATE_RESET;
        
        case (State)
            STATE_INIT:   begin                              
                write_reset <= 1'b1;
                read_reset <= 1'b1;
                write_enable <= 1'b0;
                State_Reg <= 8'b00000001;
                if (Reset_Counter[0] == 1'b1) State <= STATE_RESET;                
            end
            
            STATE_RESET:   begin
                counter <= 0;
                counter_delay <= 0;
                write_reset <= 1'b1;
                read_reset <= 1'b1;
                write_enable <= 1'b0; 
                State_Reg <= 8'b00000010;  
                ///CVM300_SYS_RES_N <= 1'b0;             
                if (Reset_Counter[0] == 1'b0) State <= STATE_RESET_FINISHED;             
            end                                     
 
           STATE_RESET_FINISHED:   begin
                //CVM300_SYS_RES_N <= 1'b1;
                write_reset <= 1'b0;
                read_reset <= 1'b0;
                State_Reg <= 8'b00000011;                 
                State <= STATE_DELAY;                                   
            end   
                          
            STATE_DELAY:   begin
                State_Reg <= 8'b00000100;
                if (counter_delay == 16'b0000_1111_1111_1111) begin
                    State <= STATE_ENABLE_WRITING;
                    CVM300_FRAME_REQ <= 1'b1;
                end else begin
                    counter_delay <= counter_delay + 1;
                end
            end
            
             STATE_ENABLE_WRITING:   begin
                //write_enable <= 1'b1;
                State_Reg <= 8'b00000101;
                CVM300_FRAME_REQ <= 1'b0;
                State <= STATE_COUNT;
             end
                                  
             STATE_COUNT:   begin 
                State_Reg <= 8'b00000110;                 
                if (CVM300_Line_valid && CVM300_Data_valid) begin
                    counter <= counter + 1;  
                    write_enable <= 1'b1;
                    pixel_reg <= CVM300_D[9:2];
                end else begin
                    write_enable <= 1'b0;
                end                                    
                if (counter >= 640*480)  State <= STATE_FINISH;         
             end
            
             STATE_FINISH:   begin  
                 State_Reg <= 8'b00000111;                       
                 write_enable <= 1'b0;                                                           
            end

        endcase
    end    
       
    fifo_generator_0 FIFO_for_Counter_BTPipe_Interface (
        //.wr_clk(~CVM300_CLK_OUT), //SPS
        .wr_clk(FSM_Clk),
        .wr_rst(write_reset),
        .rd_clk(okClk),
        .rd_rst(read_reset),
        .din(CVM300_D[9:2]),
        .wr_en(write_enable),
        .rd_en(FIFO_read_enable),
        .dout(FIFO_data_out),
        .full(FIFO_full),
        .empty(FIFO_empty),       
        .prog_full(FIFO_BT_BlockSize_Full)        
    );
      
    okBTPipeOut CounterToPC (
        .okHE(okHE), 
        .okEH(okEHx[ 0*65 +: 65 ]),
        .ep_addr(8'ha0), 
        .ep_datain(FIFO_data_out), 
        .ep_read(FIFO_read_enable),
        .ep_blockstrobe(BT_Strobe), 
        .ep_ready(FIFO_BT_BlockSize_Full)
    );                                      
    
    ila_0 ila_test_mod (
    .clk(ILA_Clk),
    .probe0({FSM_Clk, Trigger_event}),
    .probe1(State_Reg[7:0]),
    .probe2(Data_in[15:0]),
    .probe3(counter_delay[15:0]),
    .probe4(Reset_Counter[31:0]),
    .probe5(State_spi[7:0]),
    .probe6(DATA_Counter[31:0]),
    .probe7(write_enable),
    .probe8(CVM300_SPI_EN),
    .probe9(Done),
    .probe10({CVM300_Line_valid, CVM300_Data_valid}),
    .probe11({FIFO_data_out, CVM300_CLK_IN, CVM300_CLK_OUT}),
    .probe12(FIFO_data_out[31:0]),
    .probe13(pixel_reg[7:0]),
    .probe14(CVM300_D[9:2]),
    .probe15(CVM300_D[9:0])
    
    );
    
    okWireIn wire0 (    .okHE(okHE), 
                        .ep_addr(8'h00), 
                        .ep_dataout(Data_out));
                        
    okWireIn wire1 (   .okHE(okHE), 
                        .ep_addr(8'h01), 
                        .ep_dataout(Addr));
                                               
    okWireIn wire2 (    .okHE(okHE), 
                        .ep_addr(8'h02), 
                        .ep_dataout(Read_Write));
                        
    okWireIn wire3 (    .okHE(okHE), 
                        .ep_addr(8'h03), 
                        .ep_dataout(Start));
                          
    okWireIn wire10 (   .okHE(okHE), 
                        .ep_addr(8'h04), 
                        .ep_dataout(Reset_Counter));   
                        
    okWireOut wire20 (  .okHE(okHE), 
                        .okEH(okEHx[ 1*65 +: 65 ]),
                        .ep_addr(8'h20), 
                        .ep_datain(Data_in));
                
//    okWireOut wire21 (  .okHE(okHE), 
//                        .okEH(okEHx[ 1*65 +: 65 ]),
//                        .ep_addr(8'h21), 
//                        .ep_datain(pixel_reg));
                        
//   okWireOut wire22 (  .okHE(okHE), 
//                        .okEH(okEHx[ 1*65 +: 65 ]),
//                        .ep_addr(8'h22), 
//                        .ep_datain(counter));
                           
                    
                       
          
    assign CVM300_SPI_CLK = ( Running && State_spi < 16) ? FSM_Clk : 1'b0;
    assign CVM300_CLK_IN = FSM_Clk;
    assign CVM300_SPI_EN = Running;
    
    always @* begin
       if ( State_spi == 0 ) CVM300_SPI_IN <= Read_Write;
       if ( State_spi > 0 ) CVM300_SPI_IN <= Addr[7 - State_spi];
       if ( State_spi > 7 ) CVM300_SPI_IN <= ( Read_Write ? Data_out[15 - State_spi] : 1'b0 );
       if ( State_spi > 15) CVM300_SPI_IN <= 1'b0;
    end
                            
    always @(negedge FSM_Clk) begin 
        if ( Running && State_spi < 16) State_spi <= State_spi+1;
        if ( State_spi >= 16 )  Running <= 1'b0;
        if ( !Running )  Done <= 1'b1;
           
        if ( !Running && Done ) begin
            Running <= 1'b1;
            State_spi <= 8'b0;
            Done <= 1'b0;
        end
    end  
    
    always @(posedge FSM_Clk) begin
        if ( Running && State_spi == 8'b1 ) begin
            Data_in <= 16'b0;
        end
        if ( Running && State_spi > 7 && State_spi < 16) begin
            Data_in[15 - State_spi] <= CVM300_SPI_OUT;
        end 
    end     
    
    
    
    
                                                       
endmodule
