import tkinter as tk
import sys, pickle, time, math, PIL, threading, csv, queue
import numpy as np
from PIL import Image,ImageTk, ImageOps, ImageDraw, ImageChops, ImageFilter, ImageEnhance
from scipy import ndimage
import multiprocessing as mp
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)
from matplotlib.figure import Figure

logging=False

class App(mp.Process):
    def __init__(self, *args):
        self.refB = ""
        self.exit = args[0]
        self.image_in = args[1]
        self.acc_in = args[2]
        self.motorrun_event = args[3]
        self.exposure_set = args[4]
        self.frametime = 99999999.0
        self.framecount = 0
        super(App, self).__init__()
        print("doing init")

    def callback(self):
        self.exit.set()
        self.root.quit()
        sys.exit(0)

    def runmotor(self):
        if self.activate_motor.get():
            self.motorrun_event.set()
        else:
            self.motorrun_event.clear()

    def setexposure(self):
        self.exposure_set.put(int(self.exposure.get()))

    def fillframe(self, name):
        while 1:
            self.refA = self.refB
            self.refB = PIL.ImageTk.PhotoImage(Image.frombytes("RGB", (int(640),int(480)), self.image_in.recv_bytes(), "raw"))
            self.imageout.config(image=self.refB)
            self.framecount += 1
            if( self.framecount >= 10 ):
                duration = self.framecount/(time.perf_counter()-self.frametime)
                self.framecount = 0
                self.frametime = time.perf_counter()
                self.fpstext.set("Avg. fps: {:.3f}".format(duration))

    def fillgraph(self, name):
        max_pts = 2000
        pts_each = 16
        pt_time = 99999999.0
        pt_count = 0
        update_cycle = 0
        data = np.zeros((max_pts,4), dtype=np.float64)
        while 1:
            data = np.roll(data, -1*pts_each, axis=0)
            data[-1*pts_each:] = np.frombuffer(self.acc_in.recv_bytes(), dtype=np.float64).reshape((pts_each,4))
            update_cycle += 1
            if update_cycle >= 3:
                self.X.set_data(data[:,1], data[:,0] )
                self.Y.set_data(data[:,1], data[:,3] )
                self.Z.set_data(data[:,1], data[:,2] )

                self.sub.relim()
                self.sub.autoscale_view(True,True,True)
                self.canvas.draw()
                update_cycle = 0
            pt_count += 16
            if( pt_count >= 200 ):
                speed = pt_count/(time.perf_counter()-pt_time)
                pt_count = 0
                pt_time = time.perf_counter()
                self.acctext.set("Avg. pts/sec: {:.3f}".format(speed))

    def run(self):
        self.root = tk.Tk()
        self.root.protocol("WM_DELETE_WINDOW", self.callback)

        self.top = tk.Frame(self.root)
        self.top.pack(fill=tk.X, pady=10)

        tk.Label(self.top, text="Exposure: ").pack(side=tk.LEFT)
        vcmd = (self.root.register(lambda x:str.isdigit(x) or x == ""))
        self.exposure = tk.Entry(self.top, width=5, validate='all', validatecommand=(vcmd, '%P'))
        self.exposure.insert(0, "10")
        self.exposure.pack(side=tk.LEFT)
        tk.Label(self.top, text="ms").pack(side=tk.LEFT)
        tk.Button(self.top, text="SET", command=self.setexposure).pack(side=tk.LEFT)

        self.activate_motor = tk.IntVar()
        tk.Checkbutton(self.top, text="Run Motor", command=self.runmotor, variable=self.activate_motor).pack(side=tk.LEFT, padx=20)

        tk.Label(self.top, text="ECE437 Final Project!", width=80).pack(side=tk.RIGHT)


        self.middle = tk.Frame(self.root)
        self.middle.pack()
        baseimage = PIL.ImageTk.PhotoImage(Image.frombytes('RGB', (int(640),int(480)), bytes(bytearray(640*480*4*3)), 'raw'))
        self.imageout = tk.Label( self.middle, image=baseimage )
        self.imageout.pack(side=tk.LEFT)

        self.fig = Figure(figsize=(640/100, 480/100), dpi=100)
        self.sub = self.fig.add_subplot(111)
        #self.fig.set_tight_layout(True)
        
        self.X = self.sub.plot([0],[0])[0]
        self.Y = self.sub.plot([0],[0])[0]
        self.Z = self.sub.plot([0],[0])[0]

        self.X.set_label('X Axis')
        self.Y.set_label('Y Axis')
        self.Z.set_label('Z Axis')
        self.sub.legend(loc='upper left', bbox_to_anchor=(0,1.05,1,0.05), fontsize='small', ncol=3)
        self.sub.set_ylabel("Acceleration (g)")

        self.canvas = FigureCanvasTkAgg(self.fig, master=self.middle)  # A tk.DrawingArea.
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.LEFT)


        self.fpstext = tk.StringVar()
        self.fps = tk.Label(self.root, textvariable=self.fpstext, justify=tk.LEFT, anchor=tk.W)
        self.fps.pack(fill='x')

        self.acctext = tk.StringVar()
        self.acc = tk.Label(self.root, textvariable=self.acctext, justify=tk.LEFT, anchor=tk.W)
        self.acc.pack(fill='x')

        if __name__ == "__mp_main__":
            image_show = threading.Thread(target=self.fillframe, args=(1,), daemon=True)
            image_show.start()
            graph_show = threading.Thread(target=self.fillgraph, args=(1,), daemon=True)
            graph_show.start()
            self.root.mainloop()

class Reader(mp.Process):

    def __init__(self, *args):
        self.exit = args[0]
        self.pipe = args[1]
        self.pipe2 = args[2]
        self.motor = args[3]
        self.exposure_set = args[4]
        self.dev = ""
        super(Reader, self).__init__()

    def readspi(self, addr):
        self.dev.SetWireInValue(0x03, 0) # data # wire0
        self.dev.SetWireInValue(0x04, addr) # addr # wire1
        self.dev.SetWireInValue(0x05, 0) # read/write # wire2
        self.dev.SetWireInValue(0x06, 0) # wire3
        self.dev.UpdateWireIns()
        self.dev.SetWireInValue(0x06, 1) # start # wire3
        self.dev.UpdateWireIns()
        self.dev.UpdateWireOuts()
        return self.dev.GetWireOutValue(0x26) # wire20
        
    def writespi(self, addr, data):
        self.dev.SetWireInValue(0x03, data) # data # wire0
        self.dev.SetWireInValue(0x04, addr) # addr # wire1
        self.dev.SetWireInValue(0x05, 1) # read/write #wire2
        self.dev.SetWireInValue(0x06, 0) # wire3
        self.dev.UpdateWireIns()
        self.dev.SetWireInValue(0x06, 1) # start # wire3
        self.dev.UpdateWireIns()
        return

    def run_motor(self, pulses, direction):
        #print("running {}".format(pulses))
        self.dev.SetWireInValue(0x00, pulses)
        self.dev.SetWireInValue(0x01, direction)
        self.dev.SetWireInValue(0x02, 0)
        self.dev.UpdateWireIns()
        self.dev.SetWireInValue(0x02, 1)
        self.dev.UpdateWireIns()

    def set_exposure(self, time_ms):
        exposure = time_ms/1000.0
        exp = round((((exposure/(1/25E6))-(48*44)-133)/325)+1)
        self.writespi(42, exp & 0xFF) 
        self.writespi(43, exp >> 8)


    def run(self):
        print("Reader init.")
        ok_loc = 'C:\\Program Files\\Opal Kelly\\FrontPanelUSB\\API\\Python\\3.6\\x64'
        sys.path.append(ok_loc)   # add the path of the OK library
        import ok
        self.dev = ok.okCFrontPanel()  # define a device for FrontPanel communication
        SerialStatus=self.dev.OpenBySerial("")      # open USB communicaiton with the OK board
        ConfigStatus=self.dev.ConfigureFPGA(".\\imager.bit") # Configure the FPGA with this bit file
        if SerialStatus != 0 or ConfigStatus != 0:
            print("Error with FPGA.")
            sys.exit()

        self.set_exposure(10)
        
        self.writespi(69,9) # enable clock out
        self.writespi(83,187) # set pll range to 20.83<->41.67Mhz
        self.writespi(58,44)

        # bottom offset
        self.writespi(59,240)
        self.writespi(60,10)

        # PGA gain. set to 3 for 1.75x amplification
        self.writespi(80,3)

        # top offset
        self.writespi(97,240)
        self.writespi(98,10)
        #writespi(97,0)
        #writespi(98,0)

        # ADC gain
        # for 25Mhz set to 51+64
        self.writespi(100,51+64)

        self.writespi(101,98)
        self.writespi(102,34)
        self.writespi(103,64)
        self.writespi(106,94)
        self.writespi(107,110)
        self.writespi(108,91)
        self.writespi(109,82)
        self.writespi(110,80)
        self.writespi(117,91)

        # set to Parallel out mode
        self.writespi(57,3)

        # read number of frames
        self.writespi(55,1)
        s = (648*488)//1024 * 1024 # round to nearest 1024 bytes of frame
        buf = bytearray(s) 
        s2 = 128
        buf2 = bytearray(s2)

        time.sleep(1) # just in case.

        print("reading begin")
        while 1:
            
            self.dev.ActivateTriggerIn(0x52, 0)
            result = self.dev.ReadFromBlockPipeOut(0xa0, 1024, buf)
            result2 = self.dev.ReadFromBlockPipeOut(0xa1, 128, buf2)
            
            if result != s or result2 != s2:
                print("Error reading data: {}".format(result))
                sys.exit(0)
            
            allzero = True
            for b in buf:
                if b != 0:
                    allzero = False
                    break
            if allzero:
                print("Caught empty image, retrying.")
            else:
                self.pipe.send_bytes(buf)
                self.pipe2.send_bytes(buf2)

            if(self.motor.poll(0)):
                d = self.motor.recv()
                self.run_motor(d[0],d[1])

            try:
                exp = self.exposure_set.get(block=False)
                self.set_exposure(exp)
            except queue.Empty:
                pass

            
            if(self.exit.is_set()):
                return

class Grapher(mp.Process):
    def __init__(self, *args):
        self.exit = args[0]
        self.pipein = args[1]
        self.pipeout = args[2]
        super(Grapher, self).__init__()

    def run(self):
        scaling_factor = (2.0)/(2**15)
        pt_counter = 0
        while(1):
            data = np.frombuffer(self.pipein.recv_bytes(), dtype=np.int16).reshape((16,4)) # X _ Z Y
            valid = np.all(data[:,1] == 12345)
            if not valid:
                print("Accelerometer data alignment error.")
                sys.exit(0)
            bits = 16
            data = data.astype(np.float64) * scaling_factor
            data[:,1] = np.arange(pt_counter,pt_counter+16)
            pt_counter += 16
            # data now floating-point. Columns are X, Time, Z, Y
            #print(data)
            self.pipeout.send_bytes(data.tobytes())

            if(self.exit.is_set()):
                return

class Parser(mp.Process):
    def __init__(self, *args):
        self.exit = args[0]
        self.pipein = args[1]
        self.pipeout = args[2]
        self.tracking = args[3]
        super(Parser, self).__init__()

    def run(self):
        print("Parser init.")
        s = 648*488
        s = (s//1024)*1024
        buf = bytearray(s)
        np.seterr(all="print")
        prev = np.zeros((480,640), dtype=np.float32)
        hist_size = 8
        old_diff = np.zeros((hist_size,480,640), dtype=np.float32)
        active_diff = 0
        center = (640/2, 480/2)
        while 1:
            self.tracking.value = center[1]
            raw = np.reshape(np.pad(np.frombuffer(self.pipein.recv_bytes(), dtype=np.uint8),(0,648*488-s), "constant"), (-1,648))
            raw = raw[2:482,2:642].astype(np.float32)

            old_diff[active_diff] = np.abs(raw-prev)
            old_diff[active_diff][ old_diff[active_diff] < 10 ] = 0
            active_diff = (active_diff + 1) % hist_size
            diff = np.clip(np.sum(old_diff, axis=0), 0, 255)

            if( np.sum(diff) > 1000 ):
                center = ndimage.measurements.center_of_mass(diff)

            image = Image.frombytes('L', (640,480), raw.astype(np.uint8).tobytes(), 'raw')
            image = image.convert("RGB")
            image = ImageOps.autocontrast(image)
            image.filter(ImageFilter.SHARPEN)

            only_r = diff.astype(np.uint8)[:,:,np.newaxis]
            only_r = np.insert( only_r, 1, np.zeros( (480,640), dtype=np.uint8), axis=2) # insert blank green
            only_r = np.insert( only_r, 2, np.zeros( (480,640), dtype=np.uint8), axis=2) # insert blank blue

            image_bw = Image.frombytes('RGB', (640,480), only_r.tobytes(), 'raw')
            image_bw = ImageEnhance.Contrast(image_bw).enhance(1.7)

            image = PIL.ImageChops.add(image, image_bw)

            baseimage = Image.new('RGB', (640, 480), color = 'white')
            baseimage.paste(image)

            draw = ImageDraw.Draw(baseimage)
            draw.ellipse([center[1]-5,center[0]-5,center[1]+5, center[0]+5], fill="blue", outline="blue")

            prev = raw
            self.pipeout.send_bytes(baseimage.tobytes())

            if(self.exit.is_set()):
                return

class Motor(mp.Process):
    def __init__(self, *args):
        #self.pipein = args[0]
        self.exit, self.pipeout, self.tracking, self.runmotor = args
        super(Motor, self).__init__()

    def run(self):
        f,csvf = (None, None)
        if logging:
            f = open("tracking_position.csv", "w")
            csvf = csv.writer(f, delimiter=",")
        stepping_proportion = 0.33 # this is a good value when motor voltage= 4v
        begin = time.clock()
        while 1:
            if self.runmotor.wait(timeout=1):
                if logging:
                    csvf.writerow((time.clock()-begin,self.tracking.value))
                time.sleep(0.7)
                direction = int(self.tracking.value < (640/2))
                steps = stepping_proportion * np.abs(self.tracking.value - (640/2))
                try:
                    self.pipeout.send((int(steps), int(direction)))
                except (BrokenPipeError, IOError):
                    # closing...
                    pass

            if(self.exit.is_set()):
                if logging:
                    f.close()
                return

rp_out,  rp_in  = mp.Pipe(duplex=False)
ip_out,  ip_in  = mp.Pipe(duplex=False)
ip2_out, ip2_in = mp.Pipe(duplex=False)
graph_out, graph_in = mp.Pipe(duplex=False)
motor_out , motor_in = mp.Pipe(duplex=False)
exit_event = mp.Event()
motorrun_event = mp.Event()
tracking_pos = mp.Value("f", 0)
exposure_set = mp.Queue()

if __name__ == "__main__":
    p1 = App(exit_event, rp_out, graph_out, motorrun_event, exposure_set)
    p2 = Reader(exit_event, ip_in, ip2_in, motor_out, exposure_set)
    p3 = Parser(exit_event, ip_out, rp_in, tracking_pos)
    p4 = Grapher(exit_event, ip2_out, graph_in)
    p5 = Motor(exit_event, motor_in, tracking_pos, motorrun_event)
    
    [x.start() for x in (p1,p2,p3,p4,p5)]
    exit_event.wait()
    [x.close() for x in (rp_in, ip_in, ip2_in, graph_in, motor_in)]
    [x.close() for x in (rp_out, ip_out, ip2_out, graph_out, motor_out)]
    print("exiting.")
    
#cd "..\..\Users\Vassily\Desktop\ece437\ece437-working\lab12\image test"