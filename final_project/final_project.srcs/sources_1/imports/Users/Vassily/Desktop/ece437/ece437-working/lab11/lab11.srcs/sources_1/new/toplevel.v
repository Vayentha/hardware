    `timescale 1ns / 1ps

    module toplevel(
    
        input  wire [9:0] CVM300_D,
        input  wire CVM300_Line_valid,
        input  wire CVM300_Data_valid,
        input  wire CVM300_CLK_OUT,
        output wire CVM300_FRAME_REQ,
        output wire CVM300_SYS_RES_N,
        output wire CVM300_Enable_LVDS, 
        output wire CVM300_CLK_IN,
        output wire CVM300_SPI_EN,
        output wire CVM300_SPI_CLK,
        output wire CVM300_SPI_IN,
        input  wire CVM300_SPI_OUT,

        input   wire    [4:0] okUH,
        output  wire    [2:0] okHU,
        inout   wire    [31:0] okUHU,
        inout   wire    okAA,
        
        input [3:0] button,
        output [7:0] led,
        input sys_clkn,
        input sys_clkp,  
        output I2C_SCL_1,
        inout I2C_SDA_1,
        
        output PMOD_A1,
        output PMOD_A2,
        input PMOD_A3,
        
        output wire [7:0] USER_33

    );
    /*
       reg sys_clkn=1;
        wire sys_clkp;
        wire [7:0] led;
        reg [3:0] button;
    */

        wire okClk;            //These are FrontPanel wires needed to IO communication    
        wire [112:0]    okHE;  //These are FrontPanel wires needed to IO communication    
        wire [64:0]     okEH;  //These are FrontPanel wires needed to IO communication    
                 
        //This is the OK host that allows data to be sent or recived    
        okHost hostIF (
            .okUH(okUH),
            .okHU(okHU),
            .okUHU(okUHU),
            .okClk(okClk),
            .okAA(okAA),
            .okHE(okHE),
            .okEH(okEH)
        );
        
        wire master_clk;
        IBUFGDS osc_clk(
            .O(master_clk),
            .I(sys_clkp),
            .IB(sys_clkn)
        );    
        
        //Depending on the number of outgoing endpoints, adjust endPt_count accordingly.
        localparam  endPt_count = 9;
        wire [endPt_count*65-1:0] okEHx;  
        okWireOR # (.N(endPt_count)) wireOR (okEH, okEHx);
        
        
        wire  ILA_Clk, ACK_bit, FSM_Clk, TrigerEvent;    
        wire [23:0] ClkDivThreshold = 1_000;   
        wire SCL, SDA; 
        wire [7:0] State;
        wire [15:0] SDA_data;
        
        wire [15:0] acc_x;
        wire [15:0] acc_y;
        wire [15:0] acc_z;
        wire [15:0] mag_x;
        wire [15:0] mag_y;
        wire [15:0] mag_z;
       
        wire [31:0] steps;
        wire DIR;
        wire run;
        
              
        okWireOut wire20 (  .okHE(okHE), 
                            .okEH(okEHx[ 0*65 +: 65 ]),
                            .ep_addr(8'h20), 
                            .ep_datain(acc_x));
        okWireOut wire21 (  .okHE(okHE), 
                            .okEH(okEHx[ 1*65 +: 65 ]),
                            .ep_addr(8'h21), 
                            .ep_datain(acc_y));
        okWireOut wire22 (  .okHE(okHE), 
                            .okEH(okEHx[ 2*65 +: 65 ]),
                            .ep_addr(8'h22), 
                            .ep_datain(acc_z));                                                                 
        okWireOut wire23 (  .okHE(okHE), 
                            .okEH(okEHx[ 3*65 +: 65 ]),
                            .ep_addr(8'h23), 
                            .ep_datain(mag_x));  
        okWireOut wire24 (  .okHE(okHE), 
                            .okEH(okEHx[ 4*65 +: 65 ]),
                            .ep_addr(8'h24), 
                            .ep_datain(mag_y));
        okWireOut wire25 (  .okHE(okHE), 
                            .okEH(okEHx[ 5*65 +: 65 ]),
                            .ep_addr(8'h25), 
                            .ep_datain(mag_z));  
                            
        okWireIn wire0 (    .okHE(okHE), 
                            .ep_addr(8'h00), 
                            .ep_dataout(steps));
                        
        okWireIn wire1 (   .okHE(okHE), 
                            .ep_addr(8'h01), 
                            .ep_dataout(DIR));
    
        okWireIn wire2 (    .okHE(okHE), 
                            .ep_addr(8'h02), 
                            .ep_dataout(run));
   
        wire [31:0] imager_spi_data_in;
        wire [31:0] imager_spi_addr;
        wire [31:0] imager_spi_rw;
        wire [31:0] imager_spi_run;
        wire [31:0] imager_trigger;
        wire [31:0] imager_spi_data_out;
        wire [31:0] imager_fifo_out;
        wire imager_fifo_rd_clk;
        wire imager_fifo_read_enable;
        wire imager_fifo_ready;
        
        wire        acc_fifo_rd_clk;
        wire        acc_fifo_ready;
        wire [31:0] acc_fifo_out;
        wire        acc_fifo_read_enable;
        
        wire i2c_clk;
        assign USER_33[2] = i2c_clk;
        
        assign imager_fifo_rd_clk = okClk;
        assign acc_fifo_rd_clk = okClk;
    
        okWireIn wire3 (    .okHE(okHE), 
                            .ep_addr(8'h03), // former wire0
                            .ep_dataout(imager_spi_data_in));
    
        okWireIn wire4 (   .okHE(okHE), 
                            .ep_addr(8'h04), // former wire1
                            .ep_dataout(imager_spi_addr));
                                                   
        okWireIn wire5 (    .okHE(okHE), 
                            .ep_addr(8'h05),  // former wire2
                            .ep_dataout(imager_spi_rw));
                            
        okWireIn wire6 (    .okHE(okHE), 
                            .ep_addr(8'h06), // former wire3
                            .ep_dataout(imager_spi_run));
        
        okTriggerIn trigger52 (  .okHE(okHE), 
                                 .ep_addr(8'h52),
                                 .ep_clk(imager_trigger_clk), 
                                 .ep_trigger(imager_trigger));
                                               
        okWireOut wire26 (  .okHE(okHE), 
                            .okEH(okEHx[ 6*65 +: 65 ]),
                            .ep_addr(8'h26), // former wire20 
                            .ep_datain(imager_spi_data_out));
   
   
        okBTPipeOut ImagerToPC (
                            .okHE(okHE), 
                            .okEH(okEHx[ 7*65 +: 65 ]),
                            .ep_addr(8'ha0), 
                            .ep_datain(imager_fifo_out), 
                            .ep_read(imager_fifo_read_enable),
                            //.ep_blockstrobe(BT_Strobe), 
                            .ep_ready(imager_fifo_ready));
                            
        okBTPipeOut AccToPC (
                            .okHE(okHE), 
                            .okEH(okEHx[ 8*65 +: 65 ]),
                            .ep_addr(8'ha1), 
                            .ep_datain(acc_fifo_out), 
                            .ep_read(acc_fifo_read_enable),
                            //.ep_blockstrobe(BT_Strobe), 
                            .ep_ready(acc_fifo_ready));
                                                  
        assign TrigerEvent = button[3];

        I2C_Transmit I2C_Test1 (
            .button(button),
            .led(led),
            .clk(master_clk),
            .I2C_SCL_1(I2C_SCL_1),
            .I2C_SDA_1(I2C_SDA_1),             
            .FSM_Clk_reg(FSM_Clk),        
            //.ILA_Clk_reg(ILA_Clk),
            .ACK_bit(ACK_bit),
            .SCL(SCL),
            .SDA(SDA),
            .state_out(State),
            .SDA_data_out(SDA_data),
            
            .acc_x(acc_x),
            .acc_y(acc_y),
            .acc_z(acc_z),
            .mag_x(mag_x),
            .mag_y(mag_y),
            .mag_z(mag_z),
            
            .acc_fifo_rd_clk(acc_fifo_rd_clk),
            .acc_fifo_ready(acc_fifo_ready),
            .acc_fifo_out(acc_fifo_out),
            .acc_fifo_read_enable(acc_fifo_read_enable),
            
            .i2c_clk(i2c_clk)
            
            );
        /*
        //Instantiate the ILA module
        ila_0 ila_sample12 ( 
            .clk(ILA_Clk),
            .probe0({led, SDA, SCL, ACK_bit}),                             
            .probe1({FSM_Clk, TrigerEvent}),
            .probe2(State),
            .probe3(SDA_data)
            );  
        */ 
    
        motor_driver motor1 (
            .PMOD_A1(PMOD_A1),
            .PMOD_A2(PMOD_A2),
            .PMOD_A3(PMOD_A3),
            //.USER_33(USER_33),
            .clk(master_clk),
            //.led(led),
            .steps(steps),
            .DIR(DIR),
            .run(run)
        
        );
        
 
    wire image_clk;
    clk_wiz_0 clk_wiz_0 ( .clk_out1(image_clk),
                          .clk_in1(master_clk)
    );
        
    imager imager (
            .image_clk(image_clk),
            .CVM300_D(CVM300_D),
            .CVM300_Line_valid(CVM300_Line_valid),
            .CVM300_Data_valid(CVM300_Data_valid),
            .CVM300_CLK_OUT(CVM300_CLK_OUT),
            .CVM300_FRAME_REQ(CVM300_FRAME_REQ),
            .CVM300_SYS_RES_N(CVM300_SYS_RES_N),
            .CVM300_Enable_LVDS(CVM300_Enable_LVDS),

            .CVM300_CLK_IN(CVM300_CLK_IN),
            .CVM300_SPI_EN(CVM300_SPI_EN),
            .CVM300_SPI_CLK(CVM300_SPI_CLK),
            .CVM300_SPI_IN(CVM300_SPI_IN),
            .CVM300_SPI_OUT(CVM300_SPI_OUT),
    
            //.USER_33(USER_33),
    
            .imager_fifo_out(imager_fifo_out),
            .imager_fifo_rd_clk(imager_fifo_rd_clk),
            .imager_fifo_read_enable(imager_fifo_read_enable),
            .imager_fifo_ready(imager_fifo_ready),
    
            .imager_spi_data_in(imager_spi_data_in),
            .imager_spi_addr(imager_spi_addr),
            .imager_spi_rw(imager_spi_rw),
            .imager_spi_run(imager_spi_run),
            .imager_trigger(imager_trigger),
            .imager_trigger_clk(imager_trigger_clk),
            .imager_spi_data_out(imager_spi_data_out)        
        
     );

         /*
        assign sys_clkp = ~sys_clkn;    
        always begin
            #5 sys_clkn = ~sys_clkn;
        end        
          
        initial begin          
                #0 button <= 4'b1111;                                                      
                #200000 button <= 4'b0000;           
        end   
            */        
    endmodule