`timescale 1ns / 1ps


module motor_driver(

    output PMOD_A1,
    output PMOD_A2,
    input PMOD_A3,
    output [7:0] USER_33,
    input clk,
    output [7:0] led,
    
    input wire [31:0] steps,
    input wire DIR,
    input wire run

    );

    
    /*
    reg [31:0] steps;
    reg run;
    reg DIR;
    */
    reg EN = 0;
    wire FEEDBACK;
    assign PMOD_A1 = EN;
    assign PMOD_A2 = DIR;
    assign FEEDBACK = PMOD_A3;
    assign USER_33[2] = EN;

    assign led[7:1] = 0;
    assign led[0] = running;
    
    reg run_prev = 0;
    reg running = 0;
    reg [31:0] steps_taken = 0;
    reg [31:0] idle_counter = 0;
    always @(posedge clk) begin
        run_prev <= run;
        
        if( running ) begin
            idle_counter <= idle_counter + 1;
            if ( idle_counter >= 500_000 ) begin
                idle_counter <= 0;
                steps_taken <= steps_taken + 1;
                EN <= ~EN;
            end
        end else begin
            EN <= 0;
        end
        
        if( run_prev == 0 && run == 1 && running == 0 )begin
            running <= 1;

        end

        if( steps_taken >= steps ) begin
            running <= 0;
            steps_taken <= 0;
            idle_counter <= 0;
        end
        
    end
 
endmodule
