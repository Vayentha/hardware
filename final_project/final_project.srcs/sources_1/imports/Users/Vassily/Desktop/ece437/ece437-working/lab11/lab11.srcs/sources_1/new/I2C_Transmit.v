    `timescale 1ns / 1ps

    module I2C_Transmit(
        input [3:0] button,
        output [7:0] led,
        input clk,
        output I2C_SCL_1,
        inout  I2C_SDA_1,        
        output reg FSM_Clk_reg,    
        //output reg ILA_Clk_reg,
        output reg ACK_bit,
        output reg SCL,
        output reg SDA, 
        output [7:0] state_out,
        output reg [15:0] SDA_data_out,
        
        input         acc_fifo_rd_clk,
        output        acc_fifo_ready,
        output [31:0] acc_fifo_out,
        input         acc_fifo_read_enable,
         
        
        output reg[15:0] acc_x,
        output reg[15:0] acc_y,
        output reg[15:0] acc_z,
        output reg[15:0] mag_x,
        output reg[15:0] mag_y,
        output reg[15:0] mag_z,
        
        output i2c_clk
        );
        
        //Instantiate the ClockGenerator module, where three signals are generate:
        //High speed CLK signal, Low speed FSM_Clk signal     
        //wire [23:0] ClkDivThreshold = 100;   
        reg FSM_Clk = 0;//wire FSM_Clk, ILA_Clk; 
        reg [15:0] FSM_count = 0;
        always @( posedge clk) begin
            FSM_count <= FSM_count+1;
            if ( FSM_count >= 249 ) begin
                //fast 400Khz I2c
                FSM_Clk <= ~FSM_Clk;
                FSM_count <= 0;
            end
        
        end
        
        assign i2c_clk = FSM_Clk;
        
        /*
        ClockGenerator ClockGenerator1 (  .clk(clk),                                      
                                          .ClkDivThreshold(ClkDivThreshold),
                                          .FSM_Clk(FSM_Clk),                                      
                                          .ILA_Clk(ILA_Clk) );
        */                  
        reg [7:0] SingleByteData = 8'b1001_0001;
        reg [15:0] State = 39;
        reg [3:0] button_reg; 
        reg error_bit = 1'b1;     
        reg Run_scl;
        reg [15:0] SDA_data;
        reg [7:0] Data_recieved;
        reg [7:0] SCL_pulse;
           
        reg [7:0] read_count;
        reg [47:0] multiaxis_data;
        
        reg read_write; // 0 = read, 1 = write;
        
        localparam STATE_INIT       = 8'd0;    
        assign led[7] = ACK_bit;
        assign led[6] = error_bit;
        assign I2C_SCL_1 = SCL;
        assign I2C_SDA_1 = SDA; 
        
        initial  begin
            SDA_data_out = 16'd0;
            ACK_bit = 1'b1;   
            SDA_data = 16'b0;
            SCL_pulse = 8'b0;
            Run_scl = 1'b0;
        end
        
        always @(*) begin
            button_reg = ~button;  
            FSM_Clk_reg = FSM_Clk;
            //ILA_Clk_reg = ILA_Clk;   
        end   
                     
        assign state_out = State; 
        
        reg[6:0] active_device = 7'b0000000;
        reg[7:0] active_register = 8'b00000000;
        reg[7:0] data_in = 8'b00000000;
        reg[7:0] data_in_temp = 8'b00000000;
        reg[7:0] data_out = 8'b00000000;
        
        reg[6:0] acc_sensor = 7'b0011001;
        reg[6:0] mag_sensor = 7'b0011110;
        
        reg do_reset = 0;
        reg do_write = 0;
        reg clear_fifo = 0;
       
        
        
        reg[15:0] State_master = 16'b0;
              
        always @* begin
            SCL = ~Run_scl | SCL_pulse == 1 | SCL_pulse == 2;
            SDA = 1'b1;
            if ( Run_scl )  begin
                if( !read_write ) begin
                    // doing read
                    // start
                    if ( State == 0 ) SDA = 1'b0;
                    // device write 
                    if ( State > 0 ) SDA = active_device[7-State];
                    // write bit
                    if ( State == 8 ) SDA = 1'b0;
                    // ack
                    if ( State == 9 ) SDA = 1'bz;
                    // register select
                    if ( State > 9 ) SDA = active_register[17-State];
                    // ack
                    if ( State == 18 ) SDA = 1'bz;
                    // start high
                    if ( State == 19 ) SDA = 1'b1;
                    // start transition
                    if ( State == 19 && SCL_pulse > 1) SDA = 1'b0;
                    // device read (high byte)
                    if ( State > 19 ) SDA = active_device[26-State];
                    // read bit enable
                    if ( State == 27) SDA = 1'b1;
                    // ack (state 28)
                    if ( State > 27 ) SDA = 1'bz;
                    // read byte in states 29-36
                    
                    // nack
                    if ( State == 37 ) SDA = 1'd1;
                    // stop low
                    if ( State == 38 ) SDA = 1'b0;
                    // stop transition
                    if ( State == 38 && SCL_pulse > 1) SDA = 1'b1;
                    // done
                    if ( State > 38 ) SDA = 1'b1;  
                end else begin
                    // doing write
                    // start
                    if ( State == 0 ) SDA = 1'b0;
                    // device write 
                    if ( State > 0 ) SDA = active_device[7-State];
                    // write bit
                    if ( State == 8 ) SDA = 1'b0;
                    // ack
                    if ( State == 9 ) SDA = 1'bz;
                    // register select
                    if ( State > 9 ) SDA = active_register[17-State];
                    // ack
                    if ( State == 18 ) SDA = 1'bz;
                    // data
                    if ( State >= 19 ) SDA = data_out[26-State];
                    // ack
                    if ( State == 27 ) SDA = 1'bz;
                    // stop low
                    if ( State == 28 ) SDA = 1'b0;
                    // stop transition
                    if ( State == 28 && SCL_pulse > 1) SDA = 1'b1;
                    // done
                    if ( State > 28 ) SDA = 1'b1;  
                
                end
            end
        end
                         
        always @(posedge FSM_Clk) begin 
            Run_scl <= 1'b1;
            SCL_pulse <= SCL_pulse + 1;
            if ( SCL_pulse == 3 ) SCL_pulse <= 0;


            if ( State > 28 && State < 37 && SCL_pulse == 1) begin
                // read top byte
                data_in_temp[36-State] <= SDA;
            end
            
            if ( State <= 45 && SCL_pulse == 3 ) begin
                State <= State + 1;
            end
      
            if ( State >= 39 ) begin
                //Run_scl <= 0;
                data_in <= data_in_temp;
                //SCL_pulse <= 1'b1;
            end    

            if ( State >= 45 && do_reset ) begin
                State <= 0;
            end
        end          
        
        always @(negedge FSM_Clk) begin
        
            clear_fifo <= 1'b0;
            do_write <= 1'b0;
        
            if ( State_master == 1 ) begin
                // config acceleration
                clear_fifo <= 1'b1;
                active_device <= acc_sensor;
                active_register <= 8'h20;
                read_write <= 1'b1;
                data_out <= 8'b0111_0111; // 100Hz, normal mode set
            end
            if ( State_master == 2 ) begin
                // config magnetic
                active_device <= mag_sensor;
                active_register <= 8'h02;
                read_write <= 1'b1;
                data_out <= 8'b0000_0000; // continuous conversion mode
             end
             
            if ( State_master >= 3 ) begin
                // read acceleration
                active_device <= acc_sensor;
                read_write <= 1'b0;

                if ( State_master == 3) begin
                    active_register <= 8'h29;
                    acc_x[15:8] <= data_in;
                end if( State_master == 4) begin
                    active_register <= 8'h28;
                    acc_x[7:0] <= data_in; 
                end if( State_master == 5) begin
                    active_register <= 8'h2B;
                    acc_y[15:8] <= data_in;
                end if( State_master == 6) begin
                    active_register <= 8'h2A;
                    acc_y[7:0] <= data_in;              
                end if( State_master == 7) begin
                    active_register <= 8'h2D;
                    acc_z[15:8] <= data_in;      
                end if( State_master == 8) begin
                    active_register <= 8'h2C;
                    acc_z[7:0] <= data_in;
                end
            end  
              
            /*
            if ( State_master >= 9 ) begin
                // read magnetic
                active_device <= mag_sensor;
                read_write <= 1'b0;

                if    ( State_master == 9) begin
                    active_register <= 8'h03;
                    mag_x[15:8] <= data_in;
                end if( State_master == 10) begin
                    active_register <= 8'h04;
                    mag_x[7:0] <= data_in; 
                end if( State_master == 11) begin
                    active_register <= 8'h05;
                    mag_z[15:8] <= data_in;
                end if( State_master == 12) begin
                    active_register <= 8'h06;
                    mag_z[7:0] <= data_in;              
                end if( State_master == 13) begin
                    active_register <= 8'h07;
                    mag_y[15:8] <= data_in;      
                end if( State_master == 14) begin
                    active_register <= 8'h08;
                    mag_y[7:0] <= data_in;
                end
                     
            end
            */

            
            if ( State >= 45 ) begin
                // time to reset state machine
                //State <= 0;
                do_reset <= 1'b1;
                do_write <= 1'b1;
                State_master <= State_master+1;
                if (State_master == 9) begin // this might need to be 10? // if( State_master == 14 ) begin
                    State_master <= 3;
                end
            end
            

        end
        
        
        fifo_generator_1 FIFO_for_Counter_BTPipe_Interface (
            .wr_clk(~FSM_Clk),
            .wr_rst(clear_fifo),               
            .din({16'd12345, acc_x, acc_y, acc_z}),           
            .wr_en(do_write),

            .rd_clk(acc_fifo_rd_clk),
            .rd_rst(clear_fifo),
            .rd_en(acc_fifo_read_enable),
            .dout(acc_fifo_out),       
            .prog_full(acc_fifo_ready)        
        );
        
                   
    endmodule