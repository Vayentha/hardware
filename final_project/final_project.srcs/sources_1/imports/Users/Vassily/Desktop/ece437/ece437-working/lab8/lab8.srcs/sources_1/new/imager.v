`timescale 1ns / 1ps

module imager(
    input image_clk,
  
    input [9:0] CVM300_D,
    input CVM300_Line_valid,
    input CVM300_Data_valid,
    input CVM300_CLK_OUT,
    output reg CVM300_FRAME_REQ,
    output reg CVM300_SYS_RES_N,
    output CVM300_Enable_LVDS,

    output CVM300_CLK_IN,
    output CVM300_SPI_EN,
    output CVM300_SPI_CLK,
    output reg CVM300_SPI_IN,
    input CVM300_SPI_OUT,
    
    //output wire [7:0] USER_33,
    
    output wire [31:0] imager_fifo_out,
    input  wire imager_fifo_rd_clk,
    input  wire imager_fifo_read_enable,
    output wire imager_fifo_ready,
    
    input  wire [31:0] imager_spi_data_in,
    input  wire [31:0] imager_spi_addr,
    input  wire [31:0] imager_spi_rw,
    input  wire [31:0] imager_spi_run,
    input  wire [31:0] imager_trigger,
    output wire        imager_trigger_clk,
    output wire [31:0] imager_spi_data_out   
    );
      
    wire FSM_Clk, ILA_Clk; 
    assign FSM_Clk = image_clk;
                                                         
    localparam STATE_INIT                = 8'd0;
    localparam STATE_RESET               = 8'd1;   
    localparam STATE_DELAY               = 8'd2;
    localparam STATE_RESET_FINISHED      = 8'd3;
    localparam STATE_ENABLE_WRITING      = 8'd4;
    localparam STATE_COUNT               = 8'd5;
    localparam STATE_FINISH              = 8'd6;
   
    reg [7:0] pixel_reg;
   
    reg [31:0] counter = 32'd0;
    reg [15:0] counter_delay = 16'd0;
    reg [7:0] State = STATE_INIT;
    reg [7:0] led_register = 0;
    reg [3:0] button_reg, write_enable_counter;  
    reg write_reset, read_reset, write_enable;
    wire [31:0] Reset_Counter;
    //reg Reset_Counter;
    wire [31:0] DATA_Counter;    
    wire FIFO_read_enable, FIFO_BT_BlockSize_Full, FIFO_full, FIFO_empty, BT_Strobe;
    wire [31:0] FIFO_data_out;
    
    wire  Read_Write; // 0 == read, 1 == write.
    wire  [6:0] Addr;
    wire Start;
    wire Burst;
    wire [7:0] Data_out;
    reg  [15:0] Data_in;
    //wire FSM_Clk;
                    
    reg [7:0] State_spi;
    reg Running; 
    reg Done;
    wire [6:0] NextAddr;
    
    initial  begin
        State_spi = 8'b0;
        Running = 1'b0;
        Done = 1'b1;
        //Data_in = 16'b0;
    end
    
    initial begin
        write_reset <= 1'b0;
        read_reset <= 1'b0;
        write_enable <= 1'b1;   
        CVM300_FRAME_REQ <= 1'b0; 
        CVM300_SYS_RES_N <= 1'b1;
    end
    
    /*
    assign led[0] = ~FIFO_empty; 
    assign led[1] = ~FIFO_full;
    assign led[2] = ~FIFO_BT_BlockSize_Full;
    assign led[3] = ~FIFO_read_enable;  
    assign led[7] = ~read_reset;
    assign led[6] = ~write_reset;
    */
    assign CVM300_Enable_LVDS = 1'b0;
    
    wire [31:0] reverse_data;
    assign reverse_data[31:24] = FIFO_data_out[7:0];
    assign reverse_data[23:16] = FIFO_data_out[15:8];
    assign reverse_data[15:8]  = FIFO_data_out[23:16];
    assign reverse_data[7:0]   = FIFO_data_out[31:24];
                                      
    always @(negedge CVM300_CLK_OUT) begin     
       // button_reg <= ~button;   // Grab the values from the button, complement and store them in register                
        
        case (State)
            STATE_INIT:   begin   
                //CVM300_SYS_RES_N <= 1'b0;                            
                write_reset <= 1'b1;
                read_reset <= 1'b1;
                write_enable <= 1'b0;
                if (Reset_Counter[0] == 1'b1) State <= STATE_RESET;                
            end
            
            STATE_RESET:   begin
                counter <= 0;
                counter_delay <= 0;
                write_reset <= 1'b1;
                read_reset <= 1'b1;
                write_enable <= 1'b0; 
                CVM300_FRAME_REQ <= 1'b0;  
                //CVM300_SYS_RES_N <= 1'b1;          
                if (Reset_Counter[0] == 1'b0) State <= STATE_RESET_FINISHED;             
            end                                     
 
           STATE_RESET_FINISHED:   begin
                write_reset <= 1'b0;
                read_reset <= 1'b0;                 
                State <= STATE_DELAY;                                   
            end   
                          
            STATE_DELAY:   begin
                if (counter_delay >= 2 ) begin // used to be 4096
                    State <= STATE_ENABLE_WRITING;
                    CVM300_FRAME_REQ <= 1'b0;
                end else begin
                    counter_delay <= counter_delay + 1;
                end
            end
            
             STATE_ENABLE_WRITING:   begin
                //write_enable <= 1'b1;
                CVM300_FRAME_REQ <= 1'b1;
                State <= STATE_COUNT;
             end
                                  
             STATE_COUNT:   begin                
                CVM300_FRAME_REQ <= 1'b0;  
                if (CVM300_Line_valid && CVM300_Data_valid) begin
                    counter <= counter + 1;  
                    write_enable <= 1'b1;
                end else begin
                    write_enable <= 1'b0;
                end                                    
                if (counter >= 648*488)  State <= STATE_FINISH;         
             end
            
             STATE_FINISH:   begin                         
                 write_enable <= 1'b0;                                                           
            end

        endcase
        
        if (Reset_Counter[0] == 1'b1) State <= STATE_RESET;
    end    
       
    fifo_generator_0 FIFO_for_Counter_BTPipe_Interface (
        .wr_clk(~CVM300_CLK_OUT),
        .wr_rst(write_reset),
        .rd_clk(imager_fifo_rd_clk),
        .rd_rst(read_reset),
        
        .din(CVM300_D[9:2]),
        //.din(pixel_reg),
        
        .wr_en(CVM300_Line_valid && CVM300_Data_valid),
        //.wr_en(write_enable),
        
        .rd_en(imager_fifo_read_enable),
        .dout(FIFO_data_out),
        //.full(FIFO_full),
        //.empty(FIFO_empty),       
        .prog_full(imager_fifo_ready)        
    );
   
    assign Data_out = imager_spi_data_in[7:0];
    assign Addr = imager_spi_addr[6:0];
    assign Read_Write = imager_spi_rw[0];
    assign Start = imager_spi_run[0];
    assign Reset_Counter = imager_trigger;
    assign imager_spi_data_out = Data_in;
    assign imager_trigger_clk = ~CVM300_CLK_OUT;
    assign imager_fifo_out = reverse_data;
  
    assign CVM300_SPI_CLK = ( Running && State_spi < 16) ? FSM_Clk : 1'b0;
    assign CVM300_CLK_IN = FSM_Clk;
    assign CVM300_SPI_EN = Running;
    
   always @* begin
       if ( State_spi == 0 ) CVM300_SPI_IN = Read_Write;
       if ( State_spi > 0  ) CVM300_SPI_IN = Addr[7 - State_spi];
       if ( State_spi > 7  ) CVM300_SPI_IN = ( Read_Write ? Data_out[15 - State_spi] : 1'b0 );
       if ( State_spi > 15 ) CVM300_SPI_IN = 1'b0;
    end 
                
    always @(negedge FSM_Clk) begin 
        if ( Running && State_spi < 16) State_spi <= State_spi+1;
        if ( State_spi >= 16 )  Running <= 1'b0;
        if ( !Running && !Start )  Done <= 1'b1;
           
        if ( !Running && Start && Done ) begin
            Running <= 1'b1;
            State_spi <= 8'b0;
            Done <= 1'b0;
        end
    end  
    
    always @(posedge FSM_Clk) begin
        if ( Running && State_spi == 8'b1 ) begin
            Data_in <= 16'b0;
        end
        if ( Running && State_spi > 7 && State_spi < 16) begin
            Data_in[15 - State_spi] <= CVM300_SPI_OUT;
        end 
    end     
                                                       
endmodule
