# import various libraries necessery to run your Python code
import sys    # system related library
ok_loc = 'C:\\Program Files\\Opal Kelly\\FrontPanelUSB\\API\\Python\\3.6\\x64'
sys.path.append(ok_loc)   # add the path of the OK library
import ok,time,pickle,time,math,sys     # OpalKelly library
import numpy as np
from PIL import Image

#%% 
# Define FrontPanel device variable, open USB communication and
# load the bit file in the FPGA
dev = ok.okCFrontPanel()  # define a device for FrontPanel communication
SerialStatus=dev.OpenBySerial("")      # open USB communicaiton with the OK board
ConfigStatus=dev.ConfigureFPGA(".\\imager.bit") # Configure the FPGA with this bit file

# Check if FrontPanel is initialized correctly and if the bit file is loaded.
# Otherwise terminate the program
print("----------------------------------------------------")
if SerialStatus == 0:
    print ("FrontPanel host interface was successfully initialized.")
else:    
    print ("FrontPanel host interface not detected. The error code number is:" + str(int(SerialStatus)))
    print("Exiting the program.")
    sys.exit ()

if ConfigStatus == 0:
    print ("Your bit file is successfully loaded in the FPGA.")
else:
    print ("Your bit file did not load. The error code number is:" + str(int(ConfigStatus)))
    print ("Exiting the progam.")
    sys.exit ()
print("----------------------------------------------------")
print("----------------------------------------------------")

def readspi(addr):
	dev.SetWireInValue(0x00, 0) # data
	dev.SetWireInValue(0x01, addr) # addr
	dev.SetWireInValue(0x02, 0) # read/write
	dev.SetWireInValue(0x03, 0) 
	dev.UpdateWireIns()
	dev.SetWireInValue(0x03, 1) # start
	dev.UpdateWireIns()
	#time.sleep(0.01)
	dev.UpdateWireOuts()
	return dev.GetWireOutValue(0x20)
	
def writespi(addr, data):
	dev.SetWireInValue(0x00, data) # data
	dev.SetWireInValue(0x01, addr) # addr
	dev.SetWireInValue(0x02, 1) # read/write
	dev.SetWireInValue(0x03, 0) 
	dev.UpdateWireIns()
	dev.SetWireInValue(0x03, 1) # start
	dev.UpdateWireIns()
	return

#exposure = float(input("exposure ms:"))/1000
exposure = 10E-3 #seconds
exp = round((((exposure/(1/25E6))-(48*44)-133)/325)+1)

# increase the exposure time
#writespi(42,255)
#writespi(43,4)
# uncomment to set exposure time base on exposure variable
writespi(42, exp & 0xFF) 
writespi(43, exp >> 8)

frames = 100

# read multiple frames
#writespi(55,1)

images = []
print("reading begin")
start = time.perf_counter()

i = 0
while i < frames:
	dev.SetWireInValue(0x04, 1) #Reset FIFOs and counter
	dev.UpdateWireIns()  # Update the WireIns	

	s = 648*488
	s = (s//1024)*1024

	buf = bytearray(s) # close to 648*488 but it has to be a multiple of 1024

	dev.SetWireInValue(0x04, 0) #Release reset signal
	dev.UpdateWireIns()  # Update the WireIns

	result = dev.ReadFromBlockPipeOut(0xa0, 1024, buf)
	if result != s:
		print("Error: {}".format(result))
		sys.exit(0)
	
	allzero = True
	for b in buf:
		if b != 0:
			allzero = False
			break
	if allzero:
		print("Caught empty image, retrying.")
	else:
		images.append(buf)
		i += 1

stop = time.perf_counter()
print("read in:{:.3f}s".format(stop-start))

start = time.perf_counter()
output = []
for i in range(frames):
	image = Image.frombytes('L', (648,482), bytes(images[i]), 'raw')
	image_cropped = image.crop((2,2,642,482))
	image_cropped.load()
	output.append(image_cropped)
	#image_cropped.show()
	#image_cropped.save("sensor_output.png")

stop = time.perf_counter()
print("processed in:{:.3f}s".format(stop-start))

size_a = round(math.sqrt(frames))
new_im = Image.new('L', (size_a*640,math.ceil(frames/size_a)*480 ))
x_pos = 0
y_pos = 0
for i in output:
	new_im.paste(i, (x_pos,y_pos))
	x_pos += 640
	if(x_pos >= size_a*640):
		x_pos = 0
		y_pos += 480
new_im.load()
new_im.show()

#name = "frames_{:.0f}m.pkl".format(exposure*1000)
name = "frames_dark2.pkl"
with open(name, "wb") as f:
	pickle.dump(images,f)
#new_im.save("output_multi.png")